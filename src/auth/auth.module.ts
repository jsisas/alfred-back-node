import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { KeycloakAuthStrategy } from './keycloak.strategy';

@Module({
    controllers: [AuthController],
    providers: [
      AuthService,
      KeycloakAuthStrategy,
    ],
})
export class AuthModule {}

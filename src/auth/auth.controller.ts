import { Controller, Get, UseGuards, Req, Res } from '@nestjs/common';
import { KeycloakAuthStrategy } from './keycloak.strategy';
import { AuthGuard } from '@nestjs/passport';

@Controller('/auth')
export class AuthController {
    @Get('/keycloak')
    @UseGuards(AuthGuard('keycloak'))
    keycloakStrategy(@Req() req) {

    }

    @Get('/keycloak/callback')
    @UseGuards(AuthGuard('keycloak'))
    keycloakStrategyCallback(@Res() res) {
        return res.json({name: 'test'});
    }

    @Get('/keycloak/test')
    @UseGuards(AuthGuard('keycloak'))
    testLogin(@Req() req, @Res() res) {
        return res.json({response: 'Sucess'});
    }
}

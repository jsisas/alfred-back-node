import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthService } from "./auth.service";
import KeycloakStrategy from "@exlinc/keycloak-passport";
import { validate } from "@babel/types";
import Strategy from "@exlinc/keycloak-passport";

const AUTH_KEYCLOAK_CALLBACK = "https://localhost:3000/auth/keycloak/callback";
const KEYCLOAK_HOST = "http://localhost:8080/auth/realms/alfred";
const KEYCLOAK_REALM = "alfred";
const KEYCLOAK_CLIENT_ID = "test";
const KEYCLOAK_CLIENT_SECRET = "a22635be-a972-4ff0-b550-dc578b725af4";
const KEYCLOAK_TOKEN_URL =
  "http://localhost:8080/auth/realms/alfred/protocol/openid-connect/token";
const KEYCLOAK_USERINFO_URL =
  "http://localhost:8080/auth/realms/alfred/protocol/openid-connect/userinfo";

@Injectable()
export class KeycloakAuthStrategy extends PassportStrategy(
  Strategy,
  "keycloak"
) {
  constructor(private readonly authService: AuthService) {
    super(
      {
        host: KEYCLOAK_HOST,
        realm: KEYCLOAK_REALM,
        clientID: KEYCLOAK_CLIENT_ID,
        clientSecret: KEYCLOAK_CLIENT_SECRET,
        redirect_uri: "https://127.0.0.1:8080/auth/keycloak/callback",
        authorizationURL:
          "http://localhost:8080/auth/realms/alfred/protocol/openid-connect/auth",
        callbackURL: AUTH_KEYCLOAK_CALLBACK,
        tokenURL: KEYCLOAK_TOKEN_URL,
        userInfoURL: KEYCLOAK_USERINFO_URL
      },
      (req, accessToken, refreshToken, profile, done) => {
        return done(null, profile);
      },
    );
  }
}
